FROM rabbitmq:3-management

RUN apt update && apt install -y nginx python3 default-jdk python3-pip supervisor
RUN pip3 install uwsgi
